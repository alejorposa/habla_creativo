<h1 align="center">Welcome to Prueba Habla Creativo 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
  </a>
</p>

> Landing creada basada en diseño.

## Producción

* Url: 

## Git

* Gitlab: git@gitlab.com:alejorposa/habla_creativo.git

## Install

```sh
npm install
## Para instalar todas las dependencias del desarrollo. 
gulp watch
## Iniciar en modo desarrollo 
composer update
##  Compilar el proyecto para producción
```
## Frameworks / Tecnologías

* GulpJs
* Bootstrap
* Composer
* Npm


## Author

👤 **Alejandro Restrepo**

* Website: alejorestrepo.ml

## Show your support

Give a ⭐️ if this project helped you!

***