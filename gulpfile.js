let gulp = require('gulp')
let concat = require('gulp-concat')
let uglify = require('gulp-uglify')
var gutil = require('gulp-util')
let jsmin = require('gulp-jsmin')
let cssmin = require('gulp-cssmin')
let rename = require('gulp-rename')
let htmlmin = require('gulp-htmlmin')
const browsersync = require("browser-sync").create()

let reload = browserSync.reload

var paths = {
    styles: {
        src: 'dev/css/**/*.css',
        dest: 'public/assets/css/'
    },
    scripts: {
        src: 'dev/js/**/*.js',
        dest: 'public/js/'
    }
}

function js() {
    return gulp.src(paths.scripts.src)
        .pipe(jsmin())
        .pipe(gulp.dest('./public/js/'))
    //.pipe(browserSync.stream())
}


function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./"
        },
        port: 3000
    });
    done();
}

function browserSyncReload(done) {
    browsersync.reload();
    done();
}

function minifyHtml() {
    return gulp.src('dev/html/index.html')
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./'));
}

function styles() {
    return gulp.src('./dev/css/**/*.css')
        .pipe(cssmin())
        .pipe(gulp.dest('./public/assets/css'))
}

function watchFiles() {
    gulp.watch("./dev/css/**/*", styles, browserSyncReload);
    gulp.watch("./dev/js/**/*", gulp.series(js));
    gulp.watch("./dev/html/*.html", gulp.series(minifyHtml, browserSyncReload))
}

const watch = gulp.parallel(watchFiles, browserSync);
const minify = gulp.parallel(minifyHtml);
const minifyStyles = gulp.parallel(styles);
const scripts = gulp.parallel(js);

exports.minify = minify
exports.watch = watch;
exports.js = scripts
exports.minifyStyles = minifyStyles
